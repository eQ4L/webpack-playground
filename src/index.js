import _join from 'lodash/join';
// import _ from 'lodash';

function component() {
    const element = document.createElement('div');

    // element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    element.innerHTML = _join(['Hello', 'webpack'], ' ');

    return element;
}

document.body.appendChild(component());